				<style>
				/* "Mechanik" */
				.tabs {
				  display: -webkit-box;
				  display: -webkit-flex;
				  display: -ms-flexbox;
				  display: flex;
				  -webkit-flex-wrap: wrap;
				  -ms-flex-wrap: wrap;
				  flex-wrap: wrap;
				}

				.tabs label {
				  -webkit-box-ordinal-group: 2;
				  -webkit-order: 1;
				  -ms-flex-order: 1;
				  order: 1;
				  display: block;
				  cursor: pointer;
				  -webkit-transition: background ease 0.2s;
				  transition: background ease 0.2s;
				}

				.tabs .tab {
				  -webkit-box-ordinal-group: 100;
				  -webkit-order: 99;
				  -ms-flex-order: 99;
				  order: 99;
				  -webkit-box-flex: 1;
				  -webkit-flex-grow: 1;
				  -ms-flex-positive: 1;
				  flex-grow: 1;
				  width: 100%;
				  display: none;
				}

				.tabs input[type="radio"] {
				  position: absolute;
				  opacity: 0;
				}

				.tabs input[type="radio"]:checked + label + .tab { display: block; }
				
				/* Responsiv */
				@media (max-width: 45em) {
					.tabs .tab,  .tabs label {
					  -webkit-box-ordinal-group: NaN;
					  -webkit-order: initial;
					  -ms-flex-order: initial;
					  order: initial;
					}

					.tabs label {
					  width: 100%;
					  margin-right: 0;
					  margin-top: 0.2rem;
					}
				}
				
				/* Styling */
				.tabs label {
					background: #90CAF9;
					font-weight: bold;
					padding: 1rem 2rem;
				    margin-right: 0.2rem;
				}
				
				.tabs input[type="radio"]:checked + label { 
					background: #fafafa;
				}
				
				.tabs .tab {
					background: #fafafa;
					padding: 1rem;
					text-align: left;
				}
				</style>
                <?php
				define('STATION', 'qg-club');
				$days = array( "mon" => "Montag", "tue" => "Dienstag", "wed" => "Mittwoch", "thu" => "Donnerstag", "fri" => "Freitag", "sat" => "Samstag", "sun" => "Sonntag" );
				
				function get_station_data() {
					$json = file_get_contents('http://api.laut.fm/station/' . STATION);
					$obj = json_decode($json);
					
					return $obj;
				}
				
				function get_song_data(){
					$json = file_get_contents('http://api.laut.fm/station/' . STATION . '/current_song');
					$obj = json_decode($json);
					
					return $obj;
				}
				
				function get_schedule_data(){
					$json = file_get_contents('http://api.laut.fm/station/' . STATION . '/schedule');
					$obj = json_decode($json);
					
					return $obj;
				}
				
				function get_schedules() {
					$schedule = get_schedule_data();
					
					$current_day = "";
					$schedules = array();
					foreach ($schedule as $entry) {
						if ($entry->day != $current_day){
						 $schedules[$entry->day] = array();
						 $current_day = $entry->day;
						}
						$the_schedule = array();
						$the_schedule['start'] = $entry->hour;
						$the_schedule['end'] = $entry->end_time;
						$the_schedule['name'] = $entry->name;
						
						$schedules[$entry->day][] = $the_schedule;
					}
					
					return $schedules;
				}
				
				$schedules = get_schedules();
				?>
                
				<div class="tabs">
					<?php
					$i = 0;
					foreach($schedules as $day => $the_schedules):
						echo '<input type="radio" id="tab'.$i.'" name="tab"';
						if($i == 0){
							echo '  checked="checked"';
						}
						echo '>'."\n";
						echo '<label class="tabButton" for="tab'.$i.'">'.$days[$day].'</label>'."\n";
						echo '<div class="tab">'."\n";
						
						foreach($the_schedules as $schedule) {
							echo "\t".'<p>Von ';
							if(intval($schedule['start']) < 10){
								echo '0';
							}
							echo $schedule['start'].':00';
							echo ' Uhr bis ';	
							if(intval($schedule['end']) < 10){
								echo '0';
							}
							echo $schedule['end'].':00';
							echo ' Uhr: ';
							echo $schedule['name'];
							echo '</p>'."\n";
						}
	
						echo '</div>'."\n";
						
						$i++;
					endforeach;  
					?>
				</div>
