<?php
/////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/// Aenderungen sind lediglich zwischen " und " vorzunehmen. Sonst kann es boese enden \\\
/////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


// Bitte ergaenze nur die Zeilen: 11, 14, 17, 20 und 23 aus.


// Stationsname:
$station = "radio-akustiko";

// Hier kommt deine URL mit kompletten Pfadweg zum Ordner der Bilder rein.
$pfadweg = "//test9462.lima-city.de/images/";

// Hier kannst du das Format des Bildes eintragen. (jpg, png, gif etc.)
$bildformat = "png";

// Hier kannst du die Hoehe des Bildes einstellen.
$hoehe = "140";

// Hier kannst du die Breite des Bildes einstellen.
$breite = "120";


//------------------------------------------------------------------------------\\
///////////////////////////// Ab hier nix mehr Aendern \\\\\\\\\\\\\\\\\\\\\\\\\\\
//------------------------------------------------------------------------------\\

?>
<?php
###############################################################################
## $Id: current-station-info.php,v 1.0 2011/04/05 14:10:00 paulisch Exp $    ##
##  ------------------------------------------------------------------------ ##
##      current-station-info.php - Example Usage of the laut.fm API          ##
##                  Copyright (c) 2011 laut.fm - Dennis Paulisch             ##
##                       <http://www.laut.fm/>                               ##
##  ------------------------------------------------------------------------ ##
##		     >>>  Bearbeitet von Paeddy  <<<		       	     ##
###############################################################################
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Test</title>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
   <link href="https://fonts.googleapis.com/css?family=Cormorant+Unicase|Shrikhand" rel="stylesheet">
   <style>
    body{font-family: 'Cormorant Unicase', serif;}
/*    h1, h4 {font-family: 'Shrikhand', cursive;}*/
  </style>
  </head>
 <body>
<?php

// Die Stations-Infos aus der API abfragen:
$json_station = file_get_contents("http://api.laut.fm/station/".$station);
// zu Debuggen einkommentieren:
// var_dump($http_response_header);
// var_dump($json_station);

// Die Anwort von JSON in einen assoziativen Object umwandeln:
$obj_station = json_decode($json_station);

// Aktuellen Song aus der API abfragen:
$json_song = file_get_contents("http://api.laut.fm/station/".$station."/current_song");
// zu Debuggen einkommentieren:
// var_dump($http_response_header);
// var_dump($json_song);

// Die Anwort von JSON in einen assoziativen Object umwandeln:
$obj_song = json_decode($json_song);

// Hier werden Leerzeichen durch bindestriche ersetzt.
$string = $obj_station->current_playlist->name;
$string = str_replace(" ", "-", $string);
?>

<div class="container">
<div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-md-3">
<?php echo('<img src='.$pfadweg.''.$string.'.'.$bildformat.' width="'.$breite.'" height="'.$hoehe.'">'); ?>
</div>
<div class="col-md-6">
<h4 style="padding-top:30px;">Aktuelle Playlist: <?php echo "<b>".$obj_station->current_playlist->name."</b>"; ?></h4>
<br >Aktueller Track:
<marquee><?php echo "<b>".$obj_song->artist->name." - ".$obj_song->title."</b>"; ?></marquee>
</div>
<div class="col-md-3">
<div style="padding-top:30px;">
  <a href="https://laut.fm/<?php echo $station; ?>" target="_blank"><button type="button" class="btn btn-secondary btn-block">Player</button></a>
  <button type="button" class="btn btn-secondary btn-block">Wunsch / Grussbox</button>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
<br><br>
<center>Dies basiert auf Bootstrap mit Google Fonts "Nur wenn ein Template mit Google Fonts aktiv"<br>Verwendete Google Font 'Cormorant Unicase', serif</center>
//<?php
// Ausgabe des Bildes:
//echo('<img src='.$pfadweg.''.$string.'.'.$bildformat.' width="'.$breite.'" height="'.$hoehe.'">'); echo "<br>";

// Ausgabe der Playliste:
//echo "Aktuelle Playlist:<br>";
//echo "<b>".$obj_station->current_playlist->name."</b>"; echo "<br>";

// Ausgabe des aktuellen Titels:
//echo "Du h&ouml;rst:<br>";
//echo "<b>".$obj_song->artist->name." - ".$obj_song->title."</b>";

//?>
 </body>
</html>
